$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar-nav a, footer a[href='#page-top']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  // $(window).scroll(function() {
  //   $(".slideanim").each(function(){
  //     var pos = $(this).offset().top;

  //     var winTop = $(window).scrollTop();
  //       if (pos < winTop + 600) {
  //         $(this).addClass("slide");
  //       }
  //   });
  // });
})

$(document).ready(function(){
  // $('a[href="#search"]').on('click', function(event) {                    
  //   $('#search').addClass('open');
  //   $('#search > form > input[type="search"]').focus();
  // });            
  // $('#search, #search button.close').on('click keyup', function(event) {
  //   if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
  //     $(this).removeClass('open');
  //   }
  // });
  $('.accordion').collapse()
  $('.accordion1').collapse()

    function toggleIcon(e) {
    $(e.target)
        .prev('.panel_head')
        .find(".more-less")
        .toggleClass('fa fa-angle-up fa fa-angle-down');
    }
    $('.carer_panel').on('hidden.bs.collapse', toggleIcon);
    $('.carer_panel').on('shown.bs.collapse', toggleIcon);


    function toggleIcon(e) {
    $(e.target)
        .prev('.about_header')
        .find(".more-less")
        .toggleClass('fa fa-angle-up fa fa-angle-down');
    }
    $('.about_panel').on('hidden.bs.collapse', toggleIcon);
    $('.about_panel').on('shown.bs.collapse', toggleIcon);



    $(".next-step").click(function (e) {

        var $active = $('.nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.nav-tabs li.active');
        prevTab($active);

    });


  $(".navbar-nav li a").click(function(event) {
    $(".navbar-collapse").collapse('hide');
  });
 

  $(".search_box").hide();
   $(".search").click(function(e){
        $(".search_box").slideToggle();
        e.stopPropagation();
    });

   $(".search_box").click(function(e){
    e.stopPropagation();
    });

    $(document).click(function(){
        $(".search_box").hide();
    });

   $(".search_box_m").hide();
   $(".m_search").click(function(e){
        $(".search_box_m").slideToggle();
        e.stopPropagation();
    });

   $(".search_box_m").click(function(e){
    e.stopPropagation();
    });

    $(document).click(function(){
        $(".search_box_m").hide();
    });

});


/*Scroll to top when arrow up clicked BEGIN*/
$(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 100) {
        $('#back2Top').fadeIn();
    } else {
        $('#back2Top').fadeOut();
    }
});
$(document).ready(function() {
    $("#back2Top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});

$(document).ready(function() {
 //   $(".rd_mr").click(function(){
 //    $(this).siblings(".some").toggleClass('descs black');
 //    if($(this).siblings(".some").hasClass('descs')){
 //      $(this).children("a").text("Learn More");
 //    }else{
 //      $(this).children("a").text("Learn Less");
 //    }
 //    });


 // $(".rd_mr1").click(function(){
   
 //    $(this).siblings(".some").toggleClass('descc black');
 //    if($(this).siblings(".some").hasClass('descc')){
 //      $(this).children("a").text("Learn More");
 //    }else{
 //      $(this).children("a").text("Learn Less");
 //    }
 //    });

 // $(".rd_mr2").click(function(){
   
 //    $(this).siblings(".some").toggleClass('desccc black');
 //    if($(this).siblings(".some").hasClass('desccc')){
 //      $(this).text("Read More");
 //    }else{
 //      $(this).text("Read Less");
 //    }
 //    });

  var owl = $('.owl_custom');
  owl.owlCarousel({
    loop: true,
    margin: 7,
    autoplay: false,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    dots:false,
    nav:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        475:{
            items:2
        },
        600:{
            items:2,
            margin: 5
        },
        1000:{
            items:1
        }
    }
  })

  var owl = $('.owl_custom1');
  owl.owlCarousel({
    loop: false,
    margin: 0,
    autoplay: false,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    dots:false,
    nav:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
  })

  //   var owl = $('.owl_custom2');
  // owl.owlCarousel({
  //   loop: false,
  //   margin: 10,
  //   autoplay: true,
  //   autoplayTimeout: 5000,
  //   autoplayHoverPause: true,
  //   dots:false,
    
  //   responsiveClass:true,
  //   responsive:{
  //       0:{
  //           items:1,
  //           nav:true
  //       },
  //       600:{
  //           items:2
  //       },
  //       1000:{
  //           items:2,
  //           nav:true
  //       }
  //   }
  // })

  $(".slide-toggle").click(function(){
      $(".box").animate({
          width: "toggle",
          height: "200px"
      },"slow");
  });

});              

      
function main() {

(function () {
   'use strict';

	// Hide .navbar first
	//$(".navbar").hide();
	
	// Fade in .navbar
	$(function () {
		$(window).scroll(function () {
            // set distance user needs to scroll before we fadeIn navbar
			if ($(this).scrollTop() > 60) {
				$('.navbar').addClass('scl').fadeIn();
			} else {
				$('.navbar').removeClass('scl');
			}
		});

	
	});
	
	// Preloader */
	  	$(window).load(function() {

   	// will first fade out the loading animation 
    	$("#status").fadeOut("slow"); 

    	// will fade out the whole DIV that covers the website. 
    	$("#preloader").delay(500).fadeOut("slow").remove();      

  	}) 

   // Page scroll
  	// $('a.page-scroll').click(function() {
   //      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
   //        var target = $(this.hash);
   //        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
   //        if (target.length) {
   //          $('html,body').animate({
   //            scrollTop: target.offset().top - 40
   //          }, 900);
   //          return false;
   //        }
   //      }
   //    });

    // Show Menu on Book
    $(window).bind('scroll', function() {
        var navHeight = $(window).height() - 100;
        if ($(window).scrollTop() > navHeight) {
            $('.navbar-default').addClass('on');
        } else {
            $('.navbar-default').removeClass('on');
        }
    });

    $('body').scrollspy({ 
        target: '.navbar-default',
        offset: 80
    })

  	$(document).ready(function() {
  	    $("#testimonial").owlCarousel({
        navigation : false, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true
        });

  	});

  	// Portfolio Isotope Filter
    $(window).load(function() {
        var $container = $('.portfolio-items');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        $('.cat a').click(function() {
            $('.cat .active').removeClass('active');
            $(this).addClass('active');
            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });

    });
	
	

  // jQuery Parallax
  function initParallax() {
    $('#intro').parallax("100%", 0.3);
    //$('#services').parallax("100%", 0.3);
    $('#aboutimg').parallax("100%", 0.3);	
    //$('#testimonials').parallax("100%", 0.1);

  }
  initParallax();

  	// Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});	

}());


}
main();